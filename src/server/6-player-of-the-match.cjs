const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const jsonFilePath = "../public/output/6-player-of-the-match.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    const manOfTheMatch = matches.reduce((match, player) => {
      if (match[player.season] === undefined) {
        match[player.season] = {};
      }
      if (match[player.season][player.player_of_match]) {
        match[player.season][player.player_of_match] += 1;
      } else {
        match[player.season][player.player_of_match] = 1;
      }

      return match;
    }, {});

    let playerOfMatch = Object.entries(manOfTheMatch).reduce(
      (playerName, playerDetail) => {
        let topPlayerOfMatch = Object.entries(playerDetail[1])
          .sort((player1, player2) => {
            return player2[1] - player1[1];
          })
          .slice(0, 1);
        playerName[playerDetail[0]] = Object.fromEntries(topPlayerOfMatch);

        return playerName;
      }, {});

    fs.writeFileSync(jsonFilePath, JSON.stringify(playerOfMatch));
  });
