const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const csvFilePathDeliveries = path.resolve(__dirname,"../data/deliveries.csv");
const jsonFilePath = "../public/output/4-economical-bowlers-2015.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    const season2015Id = matches
      .filter((match) => {
        return match.season.includes(2015);
      })
      .map((valueId) => {
        return valueId.id;
      });

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveries) => {
        if (deliveries === undefined) {
          return {};
        }

        const ballRuns = deliveries.reduce((batsman, delivery) => {
          if (season2015Id.includes(delivery.match_id)) {
            if (batsman[delivery.bowler]) {
              let countBall = Number(delivery.ball) <= 6 ? 1 : 0;
              batsman[delivery.bowler].run += Number(delivery.total_runs);
              batsman[delivery.bowler].balls += countBall;
              batsman[delivery.bowler].economic = (
                batsman[delivery.bowler].run /
                (batsman[delivery.bowler].balls / 6)
              ).toFixed(2);
            } else {
              batsman[delivery.bowler] = {};
              batsman[delivery.bowler].run = Number(delivery.total_runs);
              batsman[delivery.bowler].balls = 1;
            }
          }

          return batsman;
        }, {});

        const bowlerEconomy = Object.entries(ballRuns).reduce(
          (batsman, delivery) => {
            batsman[delivery[0]] = delivery[1].economic;

            return batsman;
          }, {});

        const sortedEconomy = Object.fromEntries(
          Object.entries(bowlerEconomy)
            .sort((player1, player2) => player1[1] - player2[1])
            .slice(0, 10)
        );

        fs.writeFileSync(jsonFilePath, JSON.stringify(sortedEconomy));
      });
  });
