const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const jsonFilePath = "../public/output/1-matches-per-year.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    const matchCount = matches.reduce((match, year) => {
      if (match[year.season] === undefined) {
        match[year.season] = 1;
      } else {
        match[year.season] += 1;
      }

      return match;
    }, {});

    fs.writeFileSync(jsonFilePath, JSON.stringify(matchCount));
  });
