const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const jsonFilePath = "../public/output/5-win-toss-match.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    let teamWinning = matches.reduce((teamsCount, match) => {
      if (teamsCount[match.toss_winner]) {
        if (match.toss_winner === match.winner) {
          teamsCount[match.toss_winner] += 1;
        }
      } else {
        if (match.toss_winner === match.winner) {
          teamsCount[match.toss_winner] = 1;
        }
      }
      return teamsCount;
    }, {});

    fs.writeFileSync(jsonFilePath, JSON.stringify(teamWinning));
  });
