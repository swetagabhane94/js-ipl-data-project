const fs = require("fs");
const path = require("path");
const csvFilePathDeliveries = path.resolve(__dirname, "../data/deliveries.csv");
const jsonFilePath = "../public/output/9-best-economy-bowler-superover.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathDeliveries)
  .then((deliveries) => {
    if (deliveries === undefined) {
      return {};
    }

    const bowlerEconomyRun = deliveries
      .filter((ball) => {
        return ball.is_super_over !== "0";
      })
      .reduce((superOverBowler, delivery) => {
        if (superOverBowler[delivery.bowler]) {
          superOverBowler[delivery.bowler].runs += Number(delivery.total_runs);
          superOverBowler[delivery.bowler].ball += 1;
          superOverBowler[delivery.bowler].economyValue = (
            superOverBowler[delivery.bowler].runs /
            (superOverBowler[delivery.bowler].ball / 6)
          ).toFixed(2);
        } else {
          superOverBowler[delivery.bowler] = {};
          superOverBowler[delivery.bowler].runs = Number(delivery.total_runs);
          superOverBowler[delivery.bowler].ball = 1;
        }

        return superOverBowler;
      }, {});

    const bowlerEconomy = Object.entries(bowlerEconomyRun).reduce(
      (bowlerName, bowlerEconomyValue) => {
        bowlerName[bowlerEconomyValue[0]] = bowlerEconomyValue[1].economyValue;

        return bowlerName;
      },
      {}
    );

    const bestEconomyBowler = Object.fromEntries(
      Object.entries(bowlerEconomy)
        .sort((player1, player2) => {
          return player1[1] - player2[1];
        })
        .slice(0, 1)
    );

    fs.writeFileSync(jsonFilePath, JSON.stringify(bestEconomyBowler));
  });
