const fs = require('fs');
const path = require('path')
const csvFilePathMatches = path.resolve(__dirname,'../data/matches.csv');
const csvFilePathDeliveries = path.resolve(__dirname,'../data/deliveries.csv');
const jsonFilePath = '../public/output/3-extra-run-per-team-year-2016.json';
const csv = require('csvtojson');

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    const season2016Id = matches
      .filter((match) => {
        return match.season.includes(2016);
      })
      .map((valueId) => {
        return valueId.id;
      });

    csv()
      .fromFile(csvFilePathDeliveries)
      .then((deliveries) => {
        if (deliveries === undefined) {
          return {};
        }

        const extraRunsConceded = deliveries.reduce((match, runs) => {
          if (season2016Id.includes(runs.match_id)) {
            if (match[runs.bowling_team]) {
              match[runs.bowling_team] += Number(runs.extra_runs);
            } else {
              match[runs.bowling_team] = Number(runs.extra_runs);
            }
          }
          
          return match;
        }, {});
        
        fs.writeFileSync(jsonFilePath, JSON.stringify(extraRunsConceded));
      });
  });
