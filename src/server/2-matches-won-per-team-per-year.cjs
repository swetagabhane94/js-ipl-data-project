const fs = require("fs");
const path = require("path");
const csvFilePathMatches = path.resolve(__dirname, "../data/matches.csv");
const jsonFilePath = "../public/output/2-matches-won-per-team-per-year.json";
const csv = require("csvtojson");

csv()
  .fromFile(csvFilePathMatches)
  .then((matches) => {
    if (matches === undefined) {
      return {};
    }

    const matchWins = matches.reduce((years, winTeam) => {
      if (years[winTeam.winner] === undefined) {
        years[winTeam.winner] = {};
      }

      if (years[winTeam.winner][winTeam.season]) {
        years[winTeam.winner][winTeam.season] += 1;
      } else {
        years[winTeam.winner][winTeam.season] = 1;
      }

      return years;
    }, {});

    fs.writeFileSync(jsonFilePath, JSON.stringify(matchWins));
  });
