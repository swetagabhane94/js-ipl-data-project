const { response } = require("express");
const express = require("express");
const app = express();
const path = require("path");

const problem1 = path.resolve(__dirname,"src/public/output/1-matches-per-year.json");
const problem2 = path.resolve(__dirname,"src/public/output/2-matches-won-per-team-per-year.json");
const problem3 = path.resolve(__dirname,"src/public/output/3-extra-run-per-team-year-2016.json");
const problem4 = path.resolve(__dirname,"src/public/output/4-economical-bowlers-2015.json");
const problem5 = path.resolve(__dirname,"src/public/output/5-win-toss-match.json");
const problem6 = path.resolve(__dirname,"src/public/output/6-player-of-the-match.json");
const problem7 = path.resolve(__dirname,"src/public/output/7-strike-rate-of-batsman.json");
const problem8 = path.resolve(__dirname,"src/public/output/8-highest-dismissed-player.json");
const problem9 = path.resolve(__dirname,"src/public/output/9-best-economy-bowler-superover.json");
const publicData =path.resolve(__dirname,"src/public");
const fileHTML = path.resolve(__dirname,"src/public/ipl.html");
const signHTML = path.resolve(__dirname,"src/public/login.html")

const PORT = process.env.PORT || 5001;
 

app.use(express.static(publicData));

app.get('/',(request,response)=>{
  response.sendFile(signHTML);
});

app.get('/ipl',(request,response)=>{
  response.sendFile(fileHTML);
});

app.get("/problem1", (request, response) => {
  response.sendFile(problem1);
});

app.get("/problem2", (request, response) => {
  response.sendFile(problem2);
});

app.get("/problem3", (request, response) => {
  response.sendFile(problem3);
});

app.get("/problem4", (request, response) => {
  response.sendFile(problem4);
});

app.get("/problem5", (request, response) => {
  response.sendFile(problem5);
});

app.get("/problem6", (request, response) => {
  response.sendFile(problem6);
});

app.get("/problem7", (request, response) => {
  response.sendFile(problem7);
});

app.get("/problem8", (request, response) => {
  response.sendFile(problem8);
});

app.get("/problem9", (request, response) => {
  response.sendFile(problem9);
});

app.listen(PORT, () => {
  console.log(`listening app gives http://localhost:${PORT}`);
});
